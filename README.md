**Version 0.0.1 Copyright 2018 support by ibrahim.manorek@paypro.id **


Introduction
============

This is the **Angular** version of **AdminLTE** -- what is a fully responsive admin template. Based on **[Bootstrap 3](https://github.com/twbs/bootstrap)** framework. Highly customizable and easy to use. Fits many screen resolutions from small mobile devices to large desktops. Check out the live preview now and see for yourself.

For more AdminLTE information visit  [AdminLTE.IO](https://adminlte.io/)

Installation
------------

- Clone to your machine
- Install Angular Client.
```bash
npm install -g @angular/cli
```
- Clone the repository
```bash
git clone git clone https://ibrahimmanorek91@bitbucket.org/ibrahimmanorek91/source-sevtopupper-frontend.git
```

- Install the packages
```bash
cd source-sevtopupper-frontend
npm install
```

Running the application
------------
- On the folder project
```
ng serve
```
- For starter page Navigate to [http://localhost:4200/](http://localhost:4200/)

Browser Support
---------------
- IE 9+
- Firefox (latest)
- Chrome (latest)
- Safari (latest)
- Opera (latest)

