import { NotfoundComponent } from "./notfound/notfound.component";
import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";

@NgModule({
    imports: [
      RouterModule.forRoot([
        { path: "notfound",  component: NotfoundComponent  },
        { path: "**",  redirectTo: "/notfound", pathMatch: 'full'  }
      ])
    ],
    declarations: [],
    exports: [ RouterModule]
  })
  export class MainRoutingModule { }
         