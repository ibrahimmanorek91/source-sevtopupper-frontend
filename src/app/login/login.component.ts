import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from '../global';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit, OnDestroy {
  private baseApiUrl = GlobalVariable.BASE_API_URL;
  username: string;
  password: string;
  notification: string;
  body: HTMLBodyElement = document.getElementsByTagName('body')[0];
  notifDisplay: string; 

  constructor(private http: HttpClient) { }

  ngOnInit() {
    if(sessionStorage.getItem('jUsername')!=null){
      document.location.href = 'admin/dashboard';
     }
    // add the the body classes
    this.body.classList.add('login-page');
    this.notifDisplay = 'none';
  }

  ngOnDestroy() {
    // remove the the body classes
    this.body.classList.remove('login-page');
  }

  login() {
    if((this.username==""||this.password=="")||(this.username==null||this.password==null)){
      $('.alert').show();
      this.notification = "Please Check Your Input";
      return false;
    }

    $('#btnLogin').prop('disabled',true);
    $('#btnLogin').html('Please Wait');

    const param = '{ "username": "'+this.username+'","password": "'+this.password+'" } ';
    const urls = this.baseApiUrl + '/login';
    // const urls = '//private-8d54b-ibrahimmanorek.apiary-mock.com/login';
    // console.log(urls,param)
    this.http.post(urls,param)
      .subscribe(
        data => { 
          if(data["status"] == "success"){
            sessionStorage.setItem("jUsername",data["name"]);
            sessionStorage.setItem("jIsAllowed",data["isAllowed"]);
            document.location.href = 'admin/dashboard';
          }else{
            $('.alert').show()
            this.notification = "Please check username or password !";
            $('#btnLogin').prop('disabled',false);
            $('#btnLogin').html('Sign In');
          }        
        },
        err => {
          $('.alert').show()
          this.notification = "Something wrong";
          $('#btnLogin').prop('disabled',false);
          $('#btnLogin').html('Sign In');
          console.log(err)
        }
      );
  }

}
