import { Component, OnInit } from '@angular/core';
import { GlobalVariable } from '../../global';

@Component({
  selector: 'app-admin-footer',
  templateUrl: './admin-footer.component.html',
  styleUrls: ['./admin-footer.component.css']
})
export class AdminFooterComponent implements OnInit {
  baseVersion = GlobalVariable.version;

  constructor() { }

  ngOnInit() {
  }

}
