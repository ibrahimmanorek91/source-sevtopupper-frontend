import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalVariable } from '../../global';

@Component({
  selector: 'app-admin-content',
  templateUrl: './admin-content.component.html',
  styleUrls: ['./admin-content.component.css']
})
export class AdminContentComponent implements OnInit {
  private baseApiUrl = GlobalVariable.BASE_API_URL;
  heroes: any[];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getData();
  }

  public getData(){
    const urls = this.baseApiUrl + '/history?limit=10&start=0&order=desc';
    // const urls = "//private-f035c-history6.apiary-mock.com/history";
    this.http.get(urls)
    .subscribe(
       res => { 
        this.heroes = Object.keys(res).map(it => res[it])
        console.log(this.heroes);
      },
      err => {
        console.log(err)
      }
    );
  }

}
