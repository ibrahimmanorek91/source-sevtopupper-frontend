import { Component, OnInit } from '@angular/core';
import { } from 'jquery';
import { } from 'jqueryui';
declare var AdminLTE: any;
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
declare var $: any;
import { GlobalVariable } from '../../global';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-admin-dashboard1',
  templateUrl: './admin-dashboard1.component.html',
  styleUrls: ['./admin-dashboard1.component.css']
})
export class AdminDashboard1Component implements OnInit {
  private baseApiUrl = GlobalVariable.BASE_API_URL;
  username = sessionStorage.getItem("jUsername"); 
  isAllowed = sessionStorage.getItem("jIsAllowed");
  saldoParent: String;
  saldoChild: String;
  amount:  string;
  notif: string;
  displayStyle: string;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.validateAccessLogin();

    // if(sessionStorage.getItem("flagNotif")=="OK"){
      // this.notif = "successfully";
      // $('#alertSuccess').show();
    // }
    // sessionStorage.removeItem('flagNotif');
    
    this.getBalance();
    
    this.validateInputAmount();
  }

  validateAccessLogin(){
    if(this.isAllowed!='1'){
      // this.displayStyle = 'block';
      $("#displayStyle").remove();
    }
  }

  validateInputAmount(){
    $('#amount').keypress(function() {
      $(this).val(
          $(this).val()
              .replace(/^[^123456789]*/, '') // Remove starting non-zero characters
          );
      }
    );
    $('#amount').blur(function() {
      $(this).val(
          $(this).val()
              .replace(/^[^123456789]*/, '') // Remove starting non-zero characters
              .replace(/[^\d]*/g, '') // Remove non-digit characters
          );
      }
    );
   

    $("#amount").keypress(function (e) {
      //only digit
      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          return false;
      }
    });

  }

  getBalance(){
    this.saldoParent = "";
    this.saldoChild = "";
    const urls = this.baseApiUrl + '/getBalance';
    // const urls = "//private-6b23fc-getbalance1.apiary-mock.com/getBalance";
    this.http.get(urls)
    .subscribe(
      data => {
        if(data['code'] == "00"){
          this.saldoParent = data['saldoParent'];
          this.saldoChild = data['saldoChild'];
          $('#saldoParent').val(this.saldoParent);
          $('#btnSave').css('display','block');
          // console.log(data);
        }else{
          $('#alertError').show();
          this.notif = "Something wrong, error code : "+data['code']+"";          
        }
        
      },
      err => {
        $('#alertError').show();
          this.notif = "Something wrong";
        console.log(err)
      }
    );
  }

  validationSave(){
       if($("#amount").val() == null || $("#amount").val() == ''){
        $('#alertError').show();
        this.notif = "This field is required";
        return false;
       }

       var amountTemp = parseFloat( $("#amount").val());
       var saldoParentTemp = parseFloat($("#saldoParent").val());
       if(amountTemp > saldoParentTemp){
        $('#alertError').show();
        this.notif = "Amount should not be greater than Saldo Parent";
        return false;
       }

       $('#modalShow').modal({backdrop: 'static', show: false});
       $("#modalShow").modal('show');
  }

  processSave(){
    $("#popUpValidation").html("Please Wait..");
    $("#btnSavePopUp").prop("disabled", true);
    $("#btnClosePopUp").prop("disabled", true);
    $("#btnIconPopUp").prop("disabled", true);
    const param = '{ "amount": "'+this.amount+'","sessionId":"null","name": "'+this.username+'","preBalanceParent":"'+this.saldoParent+'","preBalanceChild":"'+this.saldoChild+'" }';
    // const urls = "//private-8264c-topup2.apiary-mock.com/topup";
    const urls = this.baseApiUrl + '/topup';      
    this.serviceSave(urls, param);
    
    // this.http.post(urls,param)
    // .subscribe(
    //   data => { 
    //     if(data["status"] == "success"){
    //       setTimeout(()=>{ 
    //         window.location.href = 'admin/dashboard';
    //         sessionStorage.setItem("flagNotif","OK");
    //       }, 5000);
          
    //     }else{
    //       $('#alertError').show();
    //       this.notif = "Something wrong, error code : "+data['status']+"";
    //       $("#modalShow").modal('hide');
    //       $("#popUpValidation").html("Are you sure to save ?");
    //       $("#btnSavePopUp").prop("disabled", false);
    //       $("#btnClosePopUp").prop("disabled", false);
    //       $("#btnIconPopUp").prop("disabled", false);
    //       console.log(data);
    //     }        
    //   },
    //   err => {
    //     $("#popUpValidation").html("Are you sure to save ?");
    //     $("#btnSavePopUp").prop("disabled", false);
    //     $("#btnClosePopUp").prop("disabled", false);
    //     $("#btnIconPopUp").prop("disabled", false);
    //     $('#alertError').show();
    //     this.notif = "Something wrong";
    //     $("#modalShow").modal('hide');
    //     console.log(err);
    //   }
    // );

  }

  processSaveOK(){
    window.location.href = 'admin/dashboard';
  }

  serviceSave(url: string, param: string): Promise<any> {
    return this.http
        .post(url, param)
        .toPromise()
        .then(data => {
          if(data["status"] == "success"){
            $("#btnSavePopUp").remove();
            $("#btnClosePopUp").remove();
            $("#btnIconPopUp").remove();
            $("#popUpValidation").html("Successfully");
            $('#btnSaveOK').css('display','block');
              // window.location.href = 'admin/dashboard';
              // sessionStorage.setItem("flagNotif","OK");            
          }else{
            $('#alertError').show();
            this.notif = "Something wrong, error code : "+data['status']+"";
            $("#modalShow").modal('hide');
            $("#popUpValidation").html("Are you sure to save ?");
            $("#btnSavePopUp").prop("disabled", false);
            $("#btnClosePopUp").prop("disabled", false);
            $("#btnIconPopUp").prop("disabled", false);
            console.log(data);
          }      
        }     
        )
        .catch(err => {
          $("#popUpValidation").html("Are you sure to save ?");
          $("#btnSavePopUp").prop("disabled", false);
          $("#btnClosePopUp").prop("disabled", false);
          $("#btnIconPopUp").prop("disabled", false);
          $('#alertError').show();
          this.notif = "Something wrong";
          $("#modalShow").modal('hide');
          console.log(err);
        });
  }

}
